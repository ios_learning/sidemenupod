//
//  SideMenuVC.swift
//  SideMenuPod
//
//  Created by Anand Yadav on 18/02/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class SideMenuVC: UITableViewController {
    
    var sideMenuData = [SideMenu]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuData = createSideMenu()
        
    }
    
    func createSideMenu() -> [SideMenu] {
        var menu: [SideMenu] = []
        let menu1 = SideMenu(title: "Realtime", opened: false, subMenu:[])
        let menu2 = SideMenu(title: "Overview", opened: false, subMenu:[])
        let menu3 = SideMenu(title: "History", opened: false, subMenu:[])
        let menu4 = SideMenu(title: "Vehicles", opened: false, subMenu:[])
        let menu5 = SideMenu(title: "Job", opened: false, subMenu: ["Control Room", "Jobs", "Address Book"])
        let menu6 = SideMenu(title: "Reminders", opened: false, subMenu:["Service Reminder", "Vehicle Renewal Reminders"])
        let menu7 = SideMenu(title: "Vehicle Records", opened: false, subMenu:["Service History", "Fuel Log"])
        let menu8 = SideMenu(title: "Fuel Price", opened: false, subMenu:[])
        menu.append(menu1)
        menu.append(menu2)
        menu.append(menu3)
        menu.append(menu4)
        menu.append(menu5)
        menu.append(menu6)
        menu.append(menu7)
        menu.append(menu8)
        
        return menu
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sideMenuData.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if sideMenuData[section].opened == true {
            return sideMenuData[section].subMenu.count + 1
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell") as! SideMenuCell
            cell.title.text = sideMenuData[indexPath.section].title
            if sideMenuData[indexPath.section].subMenu.count == 0 {
                cell.dropdown.isHidden = true
            } else{
                cell.dropdown.isHidden = false
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell") as! SideMenuCell
            cell.title.text = sideMenuData[indexPath.section].subMenu[indexPath.row - 1]
            cell.dropdown.isHidden = true
//            cell.item.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            if sideMenuData[indexPath.section].opened == true {
                sideMenuData[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            } else {
                sideMenuData[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
            
            if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3
            || indexPath.section == 7 {
                dismiss(animated: false, completion: nil)
            }
            
        } else {
            dismiss(animated: false, completion: nil)
        }
    }
    

}

//extension SideMenuVC: UITableViewDataSource, UITableViewDelegate {
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return sideMenuTitle.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let menu = sideMenuTitle[indexPath.row]
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell") as! SideMenuCell
//
//        cell.setTitle(sideMenu: menu)
//
//        return cell
//    }
//}
