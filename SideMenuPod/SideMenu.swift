//
//  SideMenu.swift
//  SideMenuPod
//
//  Created by Anand Yadav on 18/02/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import Foundation
import UIKit

class SideMenu {
    var title: String
    var subMenu = [String]()
    var opened = Bool()
    
    init(title:String, opened:Bool, subMenu:[String]) {
        self.title = title
        self.opened = opened
        self.subMenu = subMenu
    }
    
}
