//
//  SideMenuCell.swift
//  SideMenuPod
//
//  Created by Anand Yadav on 18/02/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var dropdown: UIImageView!
    
    @IBOutlet weak var item: UIView!
}
